# -*- coding: utf-8 -*-

"""******************************************************************
*********************************************************************
*********************************************************************

AUTOR DEL TRABAJO:	ALFONSO MONRROY A.
CORREO:				alfonsomonrroy2012@gmail.com
Nº DE MODIFICACIÓN:	2


RESUMEN:
El archivo posee 62 comentarios en ingles los cuales fueron
traducidos a español por medio de google traductor. Las
traducciones fueron agregadas una linea despues del comentario en
ingles. Se tomaron los comentarios de una linea (#) como un 
comentario y los comentario  multilinea encerrado entre tres
comillas simples como un solo comentario.

Se indago referente a las módulos y a los elemento que estos llaman
y se agrego la referencia de la fuente donde fue odtenida la 
información por si se desea obtener mayor información referente a la
investigación realizada.

UTILIDAD DEL ARCHIVO sql_db.py en odoo
El archivo es una capa de conectividada lo que implica que en el
se encuentran los protocolos estandar de seguridad y comunicación
para transmisión de datos por la red ya que los protocolos de
comunicación permiten el intercambio entre la capa mas inferior
y los recursos mientras que los recursos de seguridad brindan
mecanismos de criptografia para identificar usuarios y recursos.


La función o utilidada del archivo fue obtenida al revisar la 
documentación propio archivo, pero en la investigación en internet
no se pudo dar referencia o solides a la siguiente información.
*********************************************************************
*********************************************************************
******************************************************************"""

# Part of Odoo. See LICENSE file for full copyright and licensing details.
# TRADUCCIÓN: Parte de Odoo. Consulte el archivo de LICENCIA para obtener todos los derechos de autor y los detalles de la licencia.

"""
The PostgreSQL connector is a connectivity layer between the OpenERP code and
the database, *not* a database abstraction toolkit. Database abstraction is what
the ORM does, in fact.
"""
"""
TRADUCCIÓN:
El conector PostgreSQL es una capa de conectividad entre el código OpenERP y
la base de datos, * no * un kit de herramientas de abstracción de base de datos.
La abstracción de la base de datos es lo que El ORM lo hace, de hecho.
"""

from contextlib import contextmanager
"""


FUENTE: https://www.ediciones-eni.com/open/mediabook.aspx?idR=1ea94923494083592154c42bbc01a85b
FUENTE: https://www.pybonacci.org/scipy-lecture-notes-ES/advanced/advanced_python/index.html
"""
from functools import wraps
"""
functools es un módulo estándar que provee una serie de funciones 
que actúan sobre otras funciones. Más específicamente pueden 
aplicarse a cualquier objeto que implemente el método __call__.
Todas las funciones del módulo son bastante diversas entre sí, 
compartiendo únicamente dicha particularidad: operan sobre otras 
funciones.

FUENTE: https://recursospython.com/guias-y-manuales/functools/
"""
import itertools
"""
Itertools es un módulo de la librería estándar de Python que incorpora
funciones que devuelven objetos iterables, es decir, estructuras de datos
basadas en elementos que pueden recorrerse secuencialmente y que pueden
utilizarse en procesos repetitivos (bucles).

Estas funciones fueron diseñadas para una ejecución rápida, haciendo un uso
eficiente de la memoria, con la idea de resolver algoritmos basados en bucles
más complicados que aquellos que habitualmente se suelen implementar en un 
programa para recorrer los elementos de una lista, diccionario, etc.

FUENTE: https://python-para-impacientes.blogspot.com/2015/08/bucles-eficientes-con-itertools.html
"""
import logging
"""
logging es un modulo para el registro de eventos - Librería estándar Python
El modulo logging nos permite rastrear un evento dentro de un software. Por ejemplo, 
registrar un mensaje de error. La ventaja de usar registros, es que se pueden configurar
para desactivar la salida o guardarse en un archivo. Esta es una ventaja sobre una
simple impresión de los errores.
El registro es un medio de seguimiento de eventos que ocurren cuando se ejecuta algún 
software. El desarrollador del software agrega las llamadas de registro a su código 
para indicar que se han producido ciertos eventos.

FUENTE: http://www.pythondiario.com/2017/12/modulo-logging-para-el-registro-de.html?m=1
"""
import time
"""
La biblioteca time contiene una serie de funciones relacionadas con la medición del tiempo.

FUENTE: http://www.mclibre.org/consultar/python/lecciones/python-biblioteca-time.html
"""
import uuid
"""
Módulo UUID de Python es utilizado para generar identificadores únicos 
universalmente UUID es un identificador universal único. También puede 
llamarlo como GUID, es decir, identificador único global.
Un UUID es un número de 128 bits o ID para identificar de forma única los 
documentos, usuarios, recursos o información en los sistemas informáticos.
UUID puede garantizar la singularidad del identificador a través del espacio
y el tiempo. cuando hablamos de espacio y tiempo, cuando UUID se genera de
acuerdo con el estándar, el identificador no duplica el que ya se ha creado
o se creará para identificar otra cosa.Por lo tanto, UUID es útil cuando se 
necesita un valor único.

FUENTE EN INGLES: https://pynative.com/python-uuid-module-to-generate-universally-unique-identifiers/
"""
import psycopg2
"""
Psycopg2 es un adaptador PostgreSQL para el lenguaje Python implementado 
utilizando libpq, la librería oficial del cliente PostgreSQL, se utiliza 
para trabajar con la base de datos PostgreSQL.
El driver psycopg2, posee un funcionamiento muy similar al del driver pg8000.
De acuerdo a la página dedicada a Python en la Wiki de PostgreSQL, el driver
psycopg2 es el más popular y utilizado por la mayoría de los framworks.

FUENTE: https://www.linuxito.com/programacion/1059-conectarse-a-bases-de-datos-postgresql-desde-python-con-psycopg2
FUENTE EN INGLES: http://zetcode.com/python/psycopg2/
"""
import psycopg2.extras
"""

"""

import psycopg2.extensions
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT, ISOLATION_LEVEL_READ_COMMITTED, ISOLATION_LEVEL_REPEATABLE_READ
"""

psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
No se inicia ninguna transacción cuando se ejecutan los comandos y no se 
requiere commit()o rollback()se requiere. Algunos comandos de PostgreSQL 
como o no pueden ejecutarse en una transacción: para ejecutar dicho uso de
comando:CREATE DATABASEVACUUM


psycopg2.extensions.ISOLATION_LEVEL_READ_UNCOMMITTED
El nivel de aislamiento se define en el estándar SQL pero no está disponible 
en el modelo MVCC de PostgreSQL: se reemplaza por el más estricto .
READ UNCOMMITTEDREAD COMMITTED


"""

from psycopg2.pool import PoolError
from werkzeug import urls
"""
Werkzeug es una completa biblioteca de aplicaciones web WSGI . 
Comenzó como una simple colección de varias utilidades para 
aplicaciones WSGI y se ha convertido en una de las bibliotecas 
de utilidades WSGI más avanzadas.

FUENTE EN INGLES: https://palletsprojects.com/p/werkzeug/
"""

psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)

_logger = logging.getLogger(__name__)

def unbuffer(symb, cr):
    if symb is None:
        return None
    return str(symb)

def undecimalize(symb, cr):
    if symb is None:
        return None
    return float(symb)

psycopg2.extensions.register_type(psycopg2.extensions.new_type((700, 701, 1700,), 'float', undecimalize))


from . import tools
from .tools.func import frame_codeinfo
from .tools import pycompat

from .tools import parse_version as pv
if pv(psycopg2.__version__) < pv('2.7'):
    from psycopg2._psycopg import QuotedString
    def adapt_string(adapted):
        """Python implementation of psycopg/psycopg2#459 from v2.7"""
		"""
		TRADUCCIÓN:
		Implementación en Python de psycopg / psycopg2 # 459 desde v2.7
		"""


        if '\x00' in adapted:
            raise ValueError("A string literal cannot contain NUL (0x00) characters.")
        return QuotedString(adapted)

    for type_ in pycompat.string_types:
        psycopg2.extensions.register_adapter(type_, adapt_string)

from datetime import timedelta
import threading
from inspect import currentframe

import re
re_from = re.compile('.* from "?([a-zA-Z_0-9]+)"? .*$')
re_into = re.compile('.* into "?([a-zA-Z_0-9]+)"? .*$')

sql_counter = 0

class Cursor(object):
    """Represents an open transaction to the PostgreSQL DB backend,
       acting as a lightweight wrapper around psycopg2's
       ``cursor`` objects.

        ``Cursor`` is the object behind the ``cr`` variable used all
        over the OpenERP code.

        .. rubric:: Transaction Isolation

        One very important property of database transactions is the
        level of isolation between concurrent transactions.
        The SQL standard defines four levels of transaction isolation,
        ranging from the most strict *Serializable* level, to the least
        strict *Read Uncommitted* level. These levels are defined in
        terms of the phenomena that must not occur between concurrent
        transactions, such as *dirty read*, etc.
        In the context of a generic business data management software
        such as OpenERP, we need the best guarantees that no data
        corruption can ever be cause by simply running multiple
        transactions in parallel. Therefore, the preferred level would
        be the *serializable* level, which ensures that a set of
        transactions is guaranteed to produce the same effect as
        running them one at a time in some order.

        However, most database management systems implement a limited
        serializable isolation in the form of
        `snapshot isolation <http://en.wikipedia.org/wiki/Snapshot_isolation>`_,
        providing most of the same advantages as True Serializability,
        with a fraction of the performance cost.
        With PostgreSQL up to version 9.0, this snapshot isolation was
        the implementation of both the ``REPEATABLE READ`` and
        ``SERIALIZABLE`` levels of the SQL standard.
        As of PostgreSQL 9.1, the previous snapshot isolation implementation
        was kept for ``REPEATABLE READ``, while a new ``SERIALIZABLE``
        level was introduced, providing some additional heuristics to
        detect a concurrent update by parallel transactions, and forcing
        one of them to rollback.

        OpenERP implements its own level of locking protection
        for transactions that are highly likely to provoke concurrent
        updates, such as stock reservations or document sequences updates.
        Therefore we mostly care about the properties of snapshot isolation,
        but we don't really need additional heuristics to trigger transaction
        rollbacks, as we are taking care of triggering instant rollbacks
        ourselves when it matters (and we can save the additional performance
        hit of these heuristics).

        As a result of the above, we have selected ``REPEATABLE READ`` as
        the default transaction isolation level for OpenERP cursors, as
        it will be mapped to the desired ``snapshot isolation`` level for
        all supported PostgreSQL version (8.3 - 9.x).

        Note: up to psycopg2 v.2.4.2, psycopg2 itself remapped the repeatable
        read level to serializable before sending it to the database, so it would
        actually select the new serializable mode on PostgreSQL 9.1. Make
        sure you use psycopg2 v2.4.2 or newer if you use PostgreSQL 9.1 and
        the performance hit is a concern for you.

        .. attribute:: cache

            Cache dictionary with a "request" (-ish) lifecycle, only lives as
            long as the cursor itself does and proactively cleared when the
            cursor is closed.

            This cache should *only* be used to store repeatable reads as it
            ignores rollbacks and savepoints, it should not be used to store
            *any* data which may be modified during the life of the cursor.

    """

    """
    TRADUCCIÓN:
    Representa una transacción abierta para el backend de PostgreSQL DB,
       Actuando como una envoltura ligera alrededor de psycopg2's
       Objetos `` cursor``.

        `` Cursor`` es el objeto detrás de la variable `` cr`` utilizada todos
        sobre el código de OpenERP.

        .. rúbrica :: Aislamiento de transacciones

        Una propiedad muy importante de las transacciones de base de datos es la
        Nivel de aislamiento entre transacciones concurrentes.
        El estándar SQL define cuatro niveles de aislamiento de transacciones,
        que van desde el nivel más estricto * serializable *, hasta el menos
        estricto * Leer nivel no confirmado *. Estos niveles se definen en
        Términos de los fenómenos que no deben ocurrir entre concurrentes.
        transacciones, como * lectura sucia *, etc.
        En el contexto de un software genérico de gestión de datos empresariales.
        Como OpenERP, necesitamos las mejores garantías de que no hay datos.
        la corrupción siempre puede ser causa simplemente ejecutando múltiples
        Transacciones en paralelo. Por lo tanto, el nivel preferido sería
        ser el nivel * serializable *, que asegura que un conjunto de
        Se garantiza que las transacciones producen el mismo efecto que
        ejecutándolos uno a la vez en algún orden.

        Sin embargo, la mayoría de los sistemas de gestión de bases de datos implementan un
        aislamiento serializable en forma de
        `aislamiento de instantáneas <http://en.wikipedia.org/wiki/Snapshot_isolation>` _,
        proporcionando la mayoría de las mismas ventajas que la verdadera serialización,
        con una fracción del costo de rendimiento.
        Con PostgreSQL hasta la versión 9.0, este aislamiento de instantáneas fue
        la implementación tanto de `` REPEATABLE READ`` como
        Niveles `` SERIALIZABLE`` del estándar SQL.
        A partir de PostgreSQL 9.1, la implementación anterior de aislamiento de instantáneas
        se guardó para `` REPEATABLE READ``, mientras que un nuevo `` SERIALIZABLE``
        nivel fue introducido, proporcionando algunas heurísticas adicionales a
        detectar una actualización concurrente por transacciones paralelas, y forzando
        Uno de ellos para revertir.

        OpenERP implementa su propio nivel de protección de bloqueo.
        para transacciones que son altamente probables de provocar concurrentes
        actualizaciones, tales como reservas de existencias o actualizaciones de secuencias de documentos.
        Por lo tanto, nos preocupamos principalmente por las propiedades del aislamiento de instantáneas,
        pero realmente no necesitamos heurísticas adicionales para activar la transacción
        Rollbacks, ya que nos encargamos de activar rollbacks instantáneos
        a nosotros mismos cuando importa (y podemos guardar el rendimiento adicional
        golpe de estas heurísticas).

        Como resultado de lo anterior, hemos seleccionado `` REPEATABLE READ`` como
        El nivel de aislamiento de transacción predeterminado para los cursores OpenERP, como
        se asignará al nivel deseado de `` aislamiento de instantánea`` para
        todas las versiones compatibles con PostgreSQL (8.3 - 9.x).

        Nota: hasta psycopg2 v.2.4.2, el propio psycopg2 reasignó lo repetible
        El nivel de lectura se puede serializar antes de enviarlo a la base de datos, por lo que
        En realidad, seleccione el nuevo modo serializable en PostgreSQL 9.1. Hacer
        Asegúrese de usar psycopg2 v2.4.2 o más reciente si usa PostgreSQL 9.1 y
        El éxito de rendimiento es una preocupación para usted.

        .. atributo :: caché

            Diccionario de caché con un ciclo de vida de "solicitud" (-ish), solo se conserva como
            siempre que el cursor en sí lo haga y se borre de forma proactiva cuando el
            el cursor esta cerrado

            Este caché debe * solo * ser usado para almacenar lecturas repetibles, ya que
            ignora las restituciones y los puntos de salvaguarda, no debe utilizarse para almacenar
            * cualquier * dato que pueda ser modificado durante la vida del cursor.

    """

    IN_MAX = 1000   # decent limit on size of IN queries - guideline = Oracle limit
    				# TRADUCCIÓN: límite decente en el tamaño de las consultas de IN - directriz = límite de Oracle

    def check(f):
        @wraps(f)
        def wrapper(self, *args, **kwargs):
            if self._closed:
                msg = 'Unable to use a closed cursor.'
                if self.__closer:
                    msg += ' It was closed at %s, line %s' % self.__closer
                raise psycopg2.OperationalError(msg)
            return f(self, *args, **kwargs)
        return wrapper

    def __init__(self, pool, dbname, dsn, serialized=True):
        self.sql_from_log = {}
        self.sql_into_log = {}

        # default log level determined at cursor creation, could be
        # TRADUCCIÓN: nivel de registro predeterminado determinado en la creación del cursor, podría ser

        # overridden later for debugging purposes
        # TRADUCCIÓN: reemplazado más adelante para fines de depuración

        self.sql_log = _logger.isEnabledFor(logging.DEBUG)

        self.sql_log_count = 0

        # avoid the call of close() (by __del__) if an exception
        # TRADUCCIÓN: evitar la llamada de close () (por __del__) si es una excepción

        # is raised by any of the following initialisations
        # TRADUCCIÓN: es levantado por cualquiera de las siguientes inicializaciones

        self._closed = True

        self.__pool = pool
        self.dbname = dbname
        # Whether to enable snapshot isolation level for this cursor.
        # 10 TRADUCCIÓN: Si se habilita el nivel de aislamiento de instantáneas para este cursor.

        # see also the docstring of Cursor.
        # TRADUCCIÓN: ver también la cadena de documentación del cursor.

        self._serialized = serialized

        self._cnx = pool.borrow(dsn)
        self._obj = self._cnx.cursor()
        if self.sql_log:
            self.__caller = frame_codeinfo(currentframe(), 2)
        else:
            self.__caller = False
        self._closed = False   # real initialisation value
        					   # TRADUCCIÓN: valor de inicialización real
        self.autocommit(False)
        self.__closer = False

        self._default_log_exceptions = True

        self.cache = {}

        # event handlers, see method after() below
        # TRADUCCIÓN: manejadores de eventos, vea el método después de () a continuación
        self._event_handlers = {'commit': [], 'rollback': []}

    def __build_dict(self, row):
        return {d.name: row[i] for i, d in enumerate(self._obj.description)}
    def dictfetchone(self):
        row = self._obj.fetchone()
        return row and self.__build_dict(row)
    def dictfetchmany(self, size):
        return [self.__build_dict(row) for row in self._obj.fetchmany(size)]
    def dictfetchall(self):
        return [self.__build_dict(row) for row in self._obj.fetchall()]

    def __del__(self):
        if not self._closed and not self._cnx.closed:
            # Oops. 'self' has not been closed explicitly.
            # TRADUCCIÓN: Oops. 'self' no se ha cerrado explícitamente.

            # The cursor will be deleted by the garbage collector,
            # TRADUCCIÓN: El recolector de basura eliminará el cursor,

            # but the database connection is not put back into the connection
            # TRADUCCIÓN: pero la conexión de la base de datos no se vuelve a poner en la conexión

            # pool, preventing some operation on the database like dropping it.
            # TRADUCCIÓN: pool, impidiendo alguna operación en la base de datos como soltarla.

            # This can also lead to a server overload.
            # TRADUCCIÓN: Esto también puede llevar a una sobrecarga del servidor.

            msg = "Cursor not closed explicitly\n"
            if self.__caller:
                msg += "Cursor was created at %s:%s" % self.__caller
            else:
                msg += "Please enable sql debugging to trace the caller."
            _logger.warning(msg)
            self._close(True)

    @check
    def execute(self, query, params=None, log_exceptions=None):
        if params and not isinstance(params, (tuple, list, dict)):
            # psycopg2's TypeError is not clear if you mess up the params
            # TRADUCCIÒN: TypeError de  psycopg2 no está claro si arruinas los parámetros

            raise ValueError("SQL query parameters should be a tuple, list or dict; got %r" % (params,))

        if self.sql_log:
            encoding = psycopg2.extensions.encodings[self.connection.encoding]
            _logger.debug("query: %s", self._obj.mogrify(query, params).decode(encoding, 'replace'))
        now = time.time()
        try:
            params = params or None
            res = self._obj.execute(query, params)
        except Exception as e:
            if self._default_log_exceptions if log_exceptions is None else log_exceptions:
                _logger.error("bad query: %s\nERROR: %s", self._obj.query or query, e)
            raise

        # simple query count is always computed
		# 10 TRADUCCIÓN: el recuento de consultas simples siempre se calcula

        self.sql_log_count += 1
        delay = (time.time() - now)
        if hasattr(threading.current_thread(), 'query_count'):
            threading.current_thread().query_count += 1
            threading.current_thread().query_time += delay

        # advanced stats only if sql_log is enabled
        # TRADUCCIÓN: estadísticas avanzadas solo si sql_log está habilitado

        if self.sql_log:
            delay *= 1E6

            res_from = re_from.match(query.lower())
            if res_from:
                self.sql_from_log.setdefault(res_from.group(1), [0, 0])
                self.sql_from_log[res_from.group(1)][0] += 1
                self.sql_from_log[res_from.group(1)][1] += delay
            res_into = re_into.match(query.lower())
            if res_into:
                self.sql_into_log.setdefault(res_into.group(1), [0, 0])
                self.sql_into_log[res_into.group(1)][0] += 1
                self.sql_into_log[res_into.group(1)][1] += delay
        return res

    def split_for_in_conditions(self, ids, size=None):
        """Split a list of identifiers into one or more smaller tuples
           safe for IN conditions, after uniquifying them."""

        """
		TRADUCCIÓN:
        	Divide una lista de identificadores en una o más tuplas más pequeñas
            Seguro para condiciones IN, después de unificarlos. """

        return tools.misc.split_every(size or self.IN_MAX, ids)

    def print_log(self):
        global sql_counter

        if not self.sql_log:
            return
        def process(type):
            sqllogs = {'from': self.sql_from_log, 'into': self.sql_into_log}
            sum = 0
            if sqllogs[type]:
                sqllogitems = sqllogs[type].items()
                _logger.debug("SQL LOG %s:", type)
                for r in sorted(sqllogitems, key=lambda k: k[1]):
                    delay = timedelta(microseconds=r[1][1])
                    _logger.debug("table: %s: %s/%s", r[0], delay, r[1][0])
                    sum += r[1][1]
                sqllogs[type].clear()
            sum = timedelta(microseconds=sum)
            _logger.debug("SUM %s:%s/%d [%d]", type, sum, self.sql_log_count, sql_counter)
            sqllogs[type].clear()
        process('from')
        process('into')
        self.sql_log_count = 0
        self.sql_log = False

    @check
    def close(self):
        return self._close(False)

    def _close(self, leak=False):
        global sql_counter

        if not self._obj:
            return

        del self.cache

        if self.sql_log:
            self.__closer = frame_codeinfo(currentframe(), 3)

        # simple query count is always computed
        # TRADUCCIÓN: el recuento de consultas simples siempre se calcula

        sql_counter += self.sql_log_count

        # advanced stats only if sql_log is enabled
        # TRADUCCIÓN: estadísticas avanzadas solo si sql_log está habilitado

        self.print_log()

        self._obj.close()

        # This force the cursor to be freed, and thus, available again. It is
        # TRADUCCIÓN: Esto obliga a que se suelte el cursor y, por lo tanto, vuelva a estar disponible. Es

        # important because otherwise we can overload the server very easily
        # TRADUCCIÓN: importante porque de lo contrario podemos sobrecargar el servidor muy fácilmente

        # because of a cursor shortage (because cursors are not garbage
        # TRADUCCIÓN: debido a una escasez de cursores (porque los cursores no son basura)

        # collected as fast as they should). The problem is probably due in
        # TRADUCCIÓN: recogido tan rápido como deberían). El problema probablemente se deba a


        # part because browse records keep a reference to the cursor.
        # TRADUCCIÓN: parte porque los registros de navegación mantienen una referencia al cursor.


        del self._obj
        self._closed = True

        # Clean the underlying connection.
        # 10 TRADUCCIÓN: Limpia la conexión subyacente.


        self._cnx.rollback()

        if leak:
            self._cnx.leaked = True
        else:
            chosen_template = tools.config['db_template']
            templates_list = tuple(set(['template0', 'template1', 'postgres', chosen_template]))
            keep_in_pool = self.dbname not in templates_list
            self.__pool.give_back(self._cnx, keep_in_pool=keep_in_pool)

    @check
    def autocommit(self, on):
        if on:
            isolation_level = ISOLATION_LEVEL_AUTOCOMMIT
        else:
            # If a serializable cursor was requested, we
            # TRADUCCIÓN: Si se solicitó un cursor serializable, nosotros

            # use the appropriate PotsgreSQL isolation level
            # TRADUCCIÓN: usar el nivel de aislamiento PotsgreSQL apropiado

            # that maps to snaphsot isolation.
            # TRADUCCIÓN: que se asigna al aislamiento de snaphsot.

            # For all supported PostgreSQL versions (8.3-9.x),
            # TRADUCCIÓN: Para todas las versiones compatibles de PostgreSQL (8.3-9.x),

            # this is currently the ISOLATION_REPEATABLE_READ.
            # TRADUCCIÓN: este es actualmente el ISOLATION_REPEATABLE_READ.

            # See also the docstring of this class.
            # TRADUCCIÓN: Véase también el docstring de esta clase.

            # NOTE: up to psycopg 2.4.2, repeatable read
            # TRADUCCIÓN: NOTA: hasta psycopg 2.4.2, lectura repetible

            #       is remapped to serializable before being
            # TRADUCCIÓN: se reasigna a serializable antes de ser

            #       sent to the database, so it is in fact
            # TRADUCCIÓN: enviado a la base de datos, por lo que es de hecho

            #       unavailable for use with pg 9.1.
            # 10 TRADUCCIÓN: no disponible para usar con la pág. 9.1.

            isolation_level = \
                ISOLATION_LEVEL_REPEATABLE_READ \
                if self._serialized \
                else ISOLATION_LEVEL_READ_COMMITTED
        self._cnx.set_isolation_level(isolation_level)

    @check
    def after(self, event, func):
        """ Register an event handler.

            :param event: the event, either `'commit'` or `'rollback'`
            :param func: a callable object, called with no argument after the
                event occurs

            Be careful when coding an event handler, since any operation on the
            cursor that was just committed/rolled back will take place in the
            next transaction that has already begun, and may still be rolled
            back or committed independently. You may consider the use of a
            dedicated temporary cursor to do some database operation.
        """

        """TRADUCCIÓN: 
        	Registre un controlador de eventos.

             : param event: el evento, ya sea `'commit'` o`' rollback'`
             : param func: un objeto invocable, llamado sin argumento después del
                 evento ocurre

             Tenga cuidado al codificar un controlador de eventos, ya que cualquier operación en el
             El cursor que se acaba de confirmar / revertir tendrá lugar en el
             próxima transacción que ya ha comenzado, y aún puede ser rodada
             De vuelta o cometido independientemente. Usted puede considerar el uso de un
             Cursor temporal dedicado a realizar alguna operación de base de datos.
         """
        self._event_handlers[event].append(func)

    def _pop_event_handlers(self):

        # return the current handlers, and reset them on self
		# TRADUCCIÓN: devolver los controladores actuales, y restablecerlos en uno mismo

        result = self._event_handlers
        self._event_handlers = {'commit': [], 'rollback': []}
        return result

    @check
    def commit(self):
        """ Perform an SQL `COMMIT`
        """
        """
			TRADUCCIÓN: 
        	Realizar un SQL` COMMIT`
         """

        result = self._cnx.commit()
        for func in self._pop_event_handlers()['commit']:
            func()
        return result

    @check
    def rollback(self):
        """ Perform an SQL `ROLLBACK`
        """
        """
			TRADUCCIÓN:
        	Realizar un SQL` ROLLBACK`
         """

        result = self._cnx.rollback()
        for func in self._pop_event_handlers()['rollback']:
            func()
        return result

    def __enter__(self):
        """ Using the cursor as a contextmanager automatically commits and
            closes it::

                with cr:
                    cr.execute(...)

                # cr is committed if no failure occurred
                # cr is closed in any case
        """

        """
			TRADUCCIÓN:
	        El uso del cursor como un administrador de contexto automáticamente confirma y
	             lo cierra ::

	                 con cr:
	                     cr.execute (...)

	                 # cr es cometido si no ocurrió ninguna falla
	                 # cr está cerrado en cualquier caso
         """
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is None:
            self.commit()
        self.close()

    @contextmanager
    @check
    def savepoint(self):
        """ context manager entering in a new savepoint"""
        """ TRADUCCIÓN: administrador de contexto entrando en un nuevo punto de rescate"""

        name = uuid.uuid1().hex
        self.execute('SAVEPOINT "%s"' % name)
        try:
            yield
        except Exception:
            self.execute('ROLLBACK TO SAVEPOINT "%s"' % name)
            raise
        else:
            self.execute('RELEASE SAVEPOINT "%s"' % name)

    @check
    def __getattr__(self, name):
        return getattr(self._obj, name)

    @property
    def closed(self):
        return self._closed


class TestCursor(object):
    """ A pseudo-cursor to be used for tests, on top of a real cursor. It keeps
        the transaction open across requests, and simulates committing, rolling
        back, and closing:

              test cursor           | queries on actual cursor
            ------------------------+---------------------------------------
              cr = TestCursor(...)  | SAVEPOINT test_cursor_N
                                    |
              cr.execute(query)     | query
                                    |
              cr.commit()           | SAVEPOINT test_cursor_N
                                    |
              cr.rollback()         | ROLLBACK TO SAVEPOINT test_cursor_N
                                    |
              cr.close()            | ROLLBACK TO SAVEPOINT test_cursor_N
                                    |

    """

    """
		TRADUCCIÓN:
    	Un pseudo-cursor que se utilizará para las pruebas, encima de un cursor real. Mantiene
         la transacción se abre en todas las solicitudes, y simula la confirmación, la rotación
         de vuelta, y cerrando:

               cursor de prueba | consultas sobre el cursor real
             ------------------------ + ------------------------- --------------
               cr = TestCursor (...) | SAVEPOINT test_cursor_N
                                     |
               cr.execute (consulta) | consulta
                                     |
               cr.commit () | SAVEPOINT test_cursor_N
                                     |
               cr.rollback () | ROLLBACK TO SAVEPOINT test_cursor_N
                                     |
               cr.close () | ROLLBACK TO SAVEPOINT test_cursor_N
                                     |

     """

    _savepoint_seq = itertools.count()

    def __init__(self, cursor, lock):
        self._closed = False
        self._cursor = cursor

        # we use a lock to serialize concurrent requests
        # TRADUCCIÓN: usamos un bloqueo para serializar solicitudes concurrentes

        self._lock = lock
        self._lock.acquire()

        # in order to simulate commit and rollback, the cursor maintains a
        # TRADUCCIÓN: para simular el compromiso y el retroceso, el cursor mantiene un

        # savepoint at its last commit
        # 10 TRADUCCIÓN: savepoint en su último commit

        self._savepoint = "test_cursor_%s" % next(self._savepoint_seq)
        self._cursor.execute('SAVEPOINT "%s"' % self._savepoint)

    def close(self):
        if not self._closed:
            self._closed = True
            self._cursor.execute('ROLLBACK TO SAVEPOINT "%s"' % self._savepoint)
            self._lock.release()

    def autocommit(self, on):
        _logger.debug("TestCursor.autocommit(%r) does nothing", on)

    def commit(self):
        self._cursor.execute('SAVEPOINT "%s"' % self._savepoint)

    def rollback(self):
        self._cursor.execute('ROLLBACK TO SAVEPOINT "%s"' % self._savepoint)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is None:
            self.commit()
        self.close()

    def __getattr__(self, name):
        value = getattr(self._cursor, name)
        if callable(value) and self._closed:
            raise psycopg2.OperationalError('Unable to use a closed cursor.')
        return value


class LazyCursor(object):
    """ A proxy object to a cursor. The cursor itself is allocated only if it is
        needed. This class is useful for cached methods, that use the cursor
        only in the case of a cache miss.
    """

    """
		TRADUCCIÓN:
    	Un objeto proxy a un cursor. El cursor se asigna solo si está
         necesario. Esta clase es útil para los métodos en caché, que usan el cursor.
         Sólo en el caso de una falta de caché.
     """

    def __init__(self, dbname=None):
        self._dbname = dbname
        self._cursor = None
        self._depth = 0

    @property
    def dbname(self):
        return self._dbname or threading.currentThread().dbname

    def __getattr__(self, name):
        cr = self._cursor
        if cr is None:
            from odoo import registry
            cr = self._cursor = registry(self.dbname).cursor()
            for _ in range(self._depth):
                cr.__enter__()
        return getattr(cr, name)

    def __enter__(self):
        self._depth += 1
        if self._cursor is not None:
            self._cursor.__enter__()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._depth -= 1
        if self._cursor is not None:
            self._cursor.__exit__(exc_type, exc_value, traceback)

class PsycoConnection(psycopg2.extensions.connection):
    pass

class ConnectionPool(object):
    """ The pool of connections to database(s)

        Keep a set of connections to pg databases open, and reuse them
        to open cursors for all transactions.

        The connections are *not* automatically closed. Only a close_db()
        can trigger that.
    """

    """
		TRADUCCIÓN:
    	El grupo de conexiones a la (s) base (s) de datos.

         Mantener un conjunto de conexiones a bases de datos pg abiertas y reutilizarlas.
         Para abrir cursores para todas las transacciones.

         Las conexiones son * no * cerradas automáticamente. Sólo un close_db ()
         puede desencadenar eso.
     """

    def locked(fun):
        @wraps(fun)
        def _locked(self, *args, **kwargs):
            self._lock.acquire()
            try:
                return fun(self, *args, **kwargs)
            finally:
                self._lock.release()
        return _locked

    def __init__(self, maxconn=64):
        self._connections = []
        self._maxconn = max(maxconn, 1)
        self._lock = threading.Lock()

    def __repr__(self):
        used = len([1 for c, u in self._connections[:] if u])
        count = len(self._connections)
        return "ConnectionPool(used=%d/count=%d/max=%d)" % (used, count, self._maxconn)

    def _debug(self, msg, *args):
        _logger.debug(('%r ' + msg), self, *args)

    @locked
    def borrow(self, connection_info):
        """
        :param dict connection_info: dict of psql connection keywords
        :rtype: PsycoConnection
        """
        """
        	TRADUCCIÓN:
         : param dict connection_info: dict de las palabras clave de conexión psql
         : rtype: PsycoConnection
         """

        # free dead and leaked connections
        # TRADUCCIÓN: conexiones muertas y filtradas gratis

        for i, (cnx, _) in tools.reverse_enumerate(self._connections):
            if cnx.closed:
                self._connections.pop(i)
                self._debug('Removing closed connection at index %d: %r', i, cnx.dsn)
                continue
            if getattr(cnx, 'leaked', False):
                delattr(cnx, 'leaked')
                self._connections.pop(i)
                self._connections.append((cnx, False))
                _logger.info('%r: Free leaked connection to %r', self, cnx.dsn)

        for i, (cnx, used) in enumerate(self._connections):
            if not used and cnx._original_dsn == connection_info:
                try:
                    cnx.reset()
                except psycopg2.OperationalError:
                    self._debug('Cannot reset connection at index %d: %r', i, cnx.dsn)

                    # psycopg2 2.4.4 and earlier do not allow closing a closed connection
                    # TRADUCCIÓN: psycopg2 2.4.4 y anteriores no permiten cerrar una conexión cerrada

                    if not cnx.closed:
                        cnx.close()
                    continue
                self._connections.pop(i)
                self._connections.append((cnx, True))
                self._debug('Borrow existing connection to %r at index %d', cnx.dsn, i)

                return cnx

        if len(self._connections) >= self._maxconn:

            # try to remove the oldest connection not used
            # TRADUCCIÓN: tratar de eliminar la conexión más antigua no utilizada

            for i, (cnx, used) in enumerate(self._connections):
                if not used:
                    self._connections.pop(i)
                    if not cnx.closed:
                        cnx.close()
                    self._debug('Removing old connection at index %d: %r', i, cnx.dsn)
                    break
            else:

                # note: this code is called only if the for loop has completed (no break)
                # TRADUCCIÓN: nota: este código se llama solo si el bucle for se ha completado (sin interrupción)

                raise PoolError('The Connection Pool Is Full')

        try:
            result = psycopg2.connect(
                connection_factory=PsycoConnection,
                **connection_info)
        except psycopg2.Error:
            _logger.info('Connection to the database failed')
            raise
        result._original_dsn = connection_info
        self._connections.append((result, True))
        self._debug('Create new connection')
        return result

    @locked
    def give_back(self, connection, keep_in_pool=True):
        self._debug('Give back connection to %r', connection.dsn)
        for i, (cnx, used) in enumerate(self._connections):
            if cnx is connection:
                self._connections.pop(i)
                if keep_in_pool:
                    self._connections.append((cnx, False))
                    self._debug('Put connection to %r in pool', cnx.dsn)
                else:
                    self._debug('Forgot connection to %r', cnx.dsn)
                    cnx.close()
                break
        else:
            raise PoolError('This connection does not belong to the pool')

    @locked
    def close_all(self, dsn=None):
        count = 0
        last = None
        for i, (cnx, used) in tools.reverse_enumerate(self._connections):
            if dsn is None or cnx._original_dsn == dsn:
                cnx.close()
                last = self._connections.pop(i)[0]
                count += 1
        _logger.info('%r: Closed %d connections %s', self, count,
                    (dsn and last and 'to %r' % last.dsn) or '')


class Connection(object):
    """ A lightweight instance of a connection to postgres
    """

    """ TRADUCCIÓN: Una instancia liviana de una conexión a postgres
     """
    def __init__(self, pool, dbname, dsn):
        self.dbname = dbname
        self.dsn = dsn
        self.__pool = pool

    def cursor(self, serialized=True):
        cursor_type = serialized and 'serialized ' or ''
        _logger.debug('create %scursor to %r', cursor_type, self.dsn)
        return Cursor(self.__pool, self.dbname, self.dsn, serialized=serialized)

    # serialized_cursor is deprecated - cursors are serialized by default
    # TRADUCCIÓN: serialized_cursor está en desuso: los cursores se serializan de forma predeterminada
    serialized_cursor = cursor

    def __bool__(self):
        raise NotImplementedError()
    __nonzero__ = __bool__

def connection_info_for(db_or_uri):
    """ parse the given `db_or_uri` and return a 2-tuple (dbname, connection_params)

    Connection params are either a dictionary with a single key ``dsn``
    containing a connection URI, or a dictionary containing connection
    parameter keywords which psycopg2 can build a key/value connection string
    (dsn) from

    :param str db_or_uri: database name or postgres dsn
    :rtype: (str, dict)
    """

    """	10 TRADUCCIÓN: 

    	analiza el` db_or_uri` dado y devuelve un 2-tuple (dbname, connection_params)

     Los parámetros de conexión son un diccionario con una sola tecla `` dsn``
     que contiene una conexión URI, o un diccionario que contiene conexión
     palabras clave de parámetros que psycopg2 puede construir una cadena de conexión clave / valor
     (dsn) de

     : param str db_or_uri: nombre de la base de datos o postgres dsn
     : rtype: (str, dict)
     """

    if db_or_uri.startswith(('postgresql://', 'postgres://')):
        
        # extract db from uri
        # TRADUCCIÓN: extraer db de uri

        us = urls.url_parse(db_or_uri)
        if len(us.path) > 1:
            db_name = us.path[1:]
        elif us.username:
            db_name = us.username
        else:
            db_name = us.hostname
        return db_name, {'dsn': db_or_uri}

    connection_info = {'database': db_or_uri}
    for p in ('host', 'port', 'user', 'password', 'sslmode'):
        cfg = tools.config['db_' + p]
        if cfg:
            connection_info[p] = cfg

    return db_or_uri, connection_info

_Pool = None

def db_connect(to, allow_uri=False):
    global _Pool
    if _Pool is None:
        _Pool = ConnectionPool(int(tools.config['db_maxconn']))

    db, info = connection_info_for(to)
    if not allow_uri and db != to:
        raise ValueError('URI connections not allowed')
    return Connection(_Pool, db, info)

def close_db(db_name):
    """ You might want to call odoo.modules.registry.Registry.delete(db_name) along this function."""
    """TRADUCCIÓN: Es posible que desee llamar a odoo.modules.registry.Registry.delete (db_name) a lo largo de esta función."""
    global _Pool
    if _Pool:
        _Pool.close_all(connection_info_for(db_name)[1])

def close_all():
    global _Pool
    if _Pool:
        _Pool.close_all()


"""
GLOSARIO DE TERMINOS:
--------------------------------------------------------------------------------------------
Cursores con PostgreSQL
Los cursores son herramientas que nos permiten manipular datos de una consulta y 
procesarlos, generalmente mediante algún lenguaje de programación. En PostgreSQL
existen dos tipos de cursores, explícitos o implícitos, dependiendo de la necesidad 
de programación se puede utilizar uno u otro.

Los cursores explícitos no son otra cosa que variables  que almacenan datos respecto 
de consultas a una o mas tablas, por lo tanto, un cursor esta necesariamente formado 
por una instrucción  SELECT y como tal, debe ser declarado como una variable.

Por otra parte, los cursores implícitos están insertos directamente en el código como 
una instrucción SELECT, sin necesidad de ser declarados previamente, generalmente 
requieren alguna variable del tipo RECORD o ROWTYPE para capturar las filas.

FUENTE: https://sqltemuco.wordpress.com/2016/06/10/cursores-con-postgresql/
----------------------------------------------------------------------------------------------

"""